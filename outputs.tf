# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "remote_state" {
  value     = data.terraform_remote_state.mongodb.outputs
  sensitive = true
}

output "remote_state_google" {
  value = data.terraform_remote_state.storage.outputs
}
