# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "dyff_api" {
  metadata {
    name = "dyff-api"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

resource "random_password" "dyff_api_signing_key" {
  length  = 128
  special = false
}

resource "random_password" "session_secret" {
  length  = 128
  special = false
}

# https://artifacthub.io/packages/helm/dyff-api/dyff-api
resource "helm_release" "dyff_api" {
  name       = "dyff-api"
  namespace  = kubernetes_namespace.dyff_api.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  version    = "0.38.1"
  chart      = "dyff-api"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    affinity = {
      podAntiAffinity = {
        requiredDuringSchedulingIgnoredDuringExecution = [{
          labelSelector = {
            matchLabels = {
              "app.kubernetes.io/instance" = "dyff-api"
              "app.kubernetes.io/name"     = "dyff-api"
            }
          }
          topologyKey = "topology.kubernetes.io/zone"
        }]
      }
    }

    auth = {
      redirectUrl = "https://${var.frontend_hostname}/auth/callback"

      mongodb = {
        database = local.mongodb.credentials["auth"].database
      }
    }

    # no-op autoscaling until resources key is populated
    autoscaling = {
      enabled     = true
      maxReplicas = 3
      minReplicas = 3
    }

    extraEnvVarsConfigMap = {
      # [uvicorn] Trust proxied headers from any IP. We think this is safe
      # because all external connections go through nginx, and we only use the
      # headers to determine whether a forwarded request should use https.
      FORWARDED_ALLOW_IPS = "*"
    }

    ingress = {
      enabled = true

      className = "nginx"

      annotations = {
        "cert-manager.io/cluster-issuer" = "letsencrypt-production"
      }

      hosts = [{
        host = var.hostname
        paths = [{
          path     = "/"
          pathType = "ImplementationSpecific"
        }]
      }]

      tls = [{
        secretName = "dyff-api-tls" # pragma: allowlist secret
        hosts = [
          var.hostname
        ]
      }]
    }

    kafka = {
      bootstrapServer = local.kafka.bootstrap_servers
      topics = {
        events = local.kafka.topics_map["dyff.workflows.events"].name
        state  = local.kafka.topics_map["dyff.workflows.state"].name
      }
    }

    query = {
      mongodb = {
        database = local.mongodb.credentials["query"].database
      }
    }

    resources = {
      limits = {
        cpu                 = "1000m"
        memory              = "4Gi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "1000m"
        memory              = "2Gi"
        "ephemeral-storage" = "50Mi"
      }
    }

    storage = {
      backend = "s3"

      s3 = {
        accessKey = local.storage.access_id

        external = {
          endpoint = local.storage.hostname
        }

        internal = {
          enabled = false
        }
      }

      urls = {
        datasets     = local.storage.buckets["datasets"].s3_url
        measurements = local.storage.buckets["measurements"].s3_url
        modules      = local.storage.buckets["modules"].s3_url
        outputs      = local.storage.buckets["outputs"].s3_url
        reports      = local.storage.buckets["reports"].s3_url
        safetycases  = local.storage.buckets["safetycases"].s3_url
      }
    }

    topologySpreadConstraints = [{
      labelSelector = {
        matchLabels = {
          "app.kubernetes.io/instance" = "dyff-api"
          "app.kubernetes.io/name"     = "dyff-api"
        }
      }
      maxSkew           = 1
      topologyKey       = "topology.kubernetes.io/zone"
      whenUnsatisfiable = "DoNotSchedule"
    }]

    workflows = {
      namespace = "workflows"
    }
  })]

  set_sensitive {
    name  = "auth.google.clientId"
    value = var.google_oidc_client_id
  }

  set_sensitive {
    name  = "auth.google.clientSecret"
    value = var.google_oidc_client_secret
  }

  set_sensitive {
    name  = "auth.mongodb.connectionString"
    value = local.mongodb.credentials["auth"].connection_string_escaped
  }

  set_sensitive {
    name  = "auth.sessionSecret"
    value = random_password.session_secret.result
  }

  set_sensitive {
    name  = "auth.signingSecret"
    value = random_password.dyff_api_signing_key.result
  }

  set_sensitive {
    name  = "query.mongodb.connectionString"
    value = local.mongodb.credentials["query"].connection_string_escaped
  }

  set_sensitive {
    name  = "storage.s3.secretKey"
    value = local.storage.secret
  }
}
