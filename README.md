# dyff-api deployment

<!-- BADGIE TIME -->

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

dyff-api deployment for the dyff-cloud cluster.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Bootstrap procedure

### Prerequisites

### Steps

- Create a service account for each environment.

- Grant the following permissions to each service account:

  - `Compute Network Viewer`

  - `Kubernetes Engine Developer`

- Create service account API keys and save each one as `credentials.json` in the
  corresponding workspace directory.

<!-- prettier-ignore-start -->
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 5.18.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.12.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.23.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.5.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 5.18.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.12.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.23.0 |
| <a name="provider_random"></a> [random](#provider\_random) | ~> 3.5.1 |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.dyff_api](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.dyff_api](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [random_password.dyff_api_signing_key](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.session_secret](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [google_client_config.provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |
| [google_container_cluster.dyff](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/container_cluster) | data source |
| [terraform_remote_state.kafka](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |
| [terraform_remote_state.mongodb](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |
| [terraform_remote_state.storage](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `string` | n/a | yes |
| <a name="input_frontend_hostname"></a> [frontend\_hostname](#input\_frontend\_hostname) | n/a | `string` | n/a | yes |
| <a name="input_google_cloud_service_account_file"></a> [google\_cloud\_service\_account\_file](#input\_google\_cloud\_service\_account\_file) | n/a | `string` | n/a | yes |
| <a name="input_google_oidc_client_id"></a> [google\_oidc\_client\_id](#input\_google\_oidc\_client\_id) | n/a | `string` | n/a | yes |
| <a name="input_google_oidc_client_secret"></a> [google\_oidc\_client\_secret](#input\_google\_oidc\_client\_secret) | n/a | `string` | n/a | yes |
| <a name="input_hostname"></a> [hostname](#input\_hostname) | n/a | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | n/a | yes |
| <a name="input_remote_state_password"></a> [remote\_state\_password](#input\_remote\_state\_password) | n/a | `string` | n/a | yes |
| <a name="input_remote_state_user"></a> [remote\_state\_user](#input\_remote\_state\_user) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_remote_state"></a> [remote\_state](#output\_remote\_state) | n/a |
| <a name="output_remote_state_google"></a> [remote\_state\_google](#output\_remote\_state\_google) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- prettier-ignore-end -->

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
