# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  cluster_name = "${var.environment}-dyff-cloud"
  deployment   = "dyff-api"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  kafka   = data.terraform_remote_state.kafka.outputs
  mongodb = data.terraform_remote_state.mongodb.outputs
  storage = data.terraform_remote_state.storage.outputs

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }
}
