# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source                            = "../.."
  google_cloud_service_account_file = var.google_cloud_service_account_file
  google_oidc_client_secret         = var.google_oidc_client_secret
  google_oidc_client_id             = var.google_oidc_client_id
  environment                       = "production"
  project                           = "production-dyff-862492"
  hostname                          = "api.dyff.io"
  frontend_hostname                 = "app.dyff.io"
  remote_state_user                 = var.remote_state_user
  remote_state_password             = var.remote_state_password
}
