# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "remote_state" {
  value     = module.root.remote_state
  sensitive = true
}
