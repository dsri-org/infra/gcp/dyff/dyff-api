# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "google_cloud_service_account_file" {
  type    = string
  default = "credentials.json"
}

variable "google_oidc_client_secret" {
  type      = string
  sensitive = true
}

variable "google_oidc_client_id" {
  type      = string
  sensitive = true
}

variable "remote_state_user" {
  type = string
}

variable "remote_state_password" {
  type = string
}
